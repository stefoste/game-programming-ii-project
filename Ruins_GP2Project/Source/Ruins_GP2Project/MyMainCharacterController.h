// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyMainCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class RUINS_GP2PROJECT_API AMyMainCharacterController : public APlayerController
{
	GENERATED_BODY()
public:
	AMyMainCharacterController();

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
		//AMyMainCharacterController* GetCharacter();

	virtual void SetupInputComponent() override;

	virtual void Jump();

	virtual void StopJumping();

	virtual void Crouch();

	virtual void StopCrouching();

	virtual void Kick();

	virtual void StopKicking();

	virtual void MoveForward(float Value);
	
	virtual void MoveRight(float Value);

	virtual void TurnAtRate(float Rate);

	virtual void LookUpAtRate(float Rate);
	
};
