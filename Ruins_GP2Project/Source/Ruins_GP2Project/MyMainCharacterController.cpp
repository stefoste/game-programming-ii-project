// Fill out your copyright notice in the Description page of Project Settings.


#include "MyMainCharacterController.h"
#include "MyPawnCharacter.h"
#include "GameFramework/Controller.h"
#include "GameFramework/Character.h"

AMyMainCharacterController::AMyMainCharacterController() {

}

//AMyMainCharacterController* AMyMainCharacterController::GetCharacter()
//{
//	return;
//}

void AMyMainCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Jump", IE_Pressed, this, &AMyMainCharacterController::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &AMyMainCharacterController::StopJumping);
	InputComponent->BindAction("Crouch", IE_Pressed, this, &AMyMainCharacterController::Crouch);
	InputComponent->BindAction("Crouch", IE_Released, this, &AMyMainCharacterController::StopCrouching);
	InputComponent->BindAction("Kick", IE_Pressed, this, &AMyMainCharacterController::Kick);
	InputComponent->BindAction("Kick", IE_Released, this, &AMyMainCharacterController::StopKicking);

	InputComponent->BindAxis("MoveForward", this, &AMyMainCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMyMainCharacterController::MoveRight);
	InputComponent->BindAxis("TurnRate", this, &AMyMainCharacterController::TurnAtRate);
	InputComponent->BindAxis("LookRate", this, &AMyMainCharacterController::LookUpAtRate);

	//AMyMainCharacterController*GetCharacter
}

void AMyMainCharacterController::Jump()
{
	if (GEngine)      GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("I am jumping"));
	
	GetCharacter()->Jump();
}

void AMyMainCharacterController::StopJumping()
{
	if (GEngine)      GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("I have stopped jumping"));
	GetCharacter()->StopJumping();
}

void AMyMainCharacterController::Crouch()
{
	if (GEngine)      GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("I am really crouching"));
	GetCharacter()->Crouch();
}

void AMyMainCharacterController::StopCrouching()
{
	if (GEngine)      GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("I am not crouching"));
	GetCharacter()->UnCrouch();
}

void AMyMainCharacterController::Kick()
{
	bool kicking = true;

	AMyPawnCharacter* characterRef = Cast<AMyPawnCharacter>(GetCharacter());
	//characterRef->Kick();
}

void AMyMainCharacterController::StopKicking()
{
	AMyPawnCharacter* characterRef = Cast<AMyPawnCharacter>(GetCharacter());
	bool kicking = false;
}

void AMyMainCharacterController::MoveForward(float Value)
{
	
}

void AMyMainCharacterController::MoveRight(float Value)
{
	
	
}

void AMyMainCharacterController::TurnAtRate(float Rate)
{
	
	AddYawInput(Rate * GetWorld()->GetDeltaSeconds());
}

void AMyMainCharacterController::LookUpAtRate(float Rate)
{
	AddPitchInput(Rate * GetWorld()->GetDeltaSeconds());
}
