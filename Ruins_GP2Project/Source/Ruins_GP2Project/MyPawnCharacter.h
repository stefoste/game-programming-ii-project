// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyPawnCharacter.generated.h"
//#include "MyPawnCharacter.h"

UCLASS()
class RUINS_GP2PROJECT_API AMyPawnCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawnCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	//virtual void Jump();

	//virtual void StopJumping();

	//virtual void Crouch();

	//virtual void StopCrouching();

	//virtual void Kick();

	//virtual void StopKicking();

	//virtual void MoveForward(float Value);

	//virtual void MoveRight(float Value);

	//virtual void TurnAtRate(float Rate);

	//virtual void LookUpAtRate(float Rate);


};
