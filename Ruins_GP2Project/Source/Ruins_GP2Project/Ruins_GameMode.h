// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Ruins_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class RUINS_GP2PROJECT_API ARuins_GameMode : public AGameModeBase
{
	GENERATED_BODY()

	public:
		ARuins_GameMode();
		virtual ~ARuins_GameMode() = default;

		UFUNCTION(BlueprintPure, Category = "Puppets")
			virtual int GetPuppets() const;

		UFUNCTION(BlueprintCallable, Category = "Puppets")
			virtual int SetPuppets(int newNumberOfPuppets);

		virtual void StartPlay();


};

