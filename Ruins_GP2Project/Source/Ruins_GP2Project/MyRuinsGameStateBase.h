// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "MyRuinsGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class RUINS_GP2PROJECT_API AMyRuinsGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
public:
	AMyRuinsGameStateBase();
	virtual ~AMyRuinsGameStateBase() = default;

	UPROPERTY(EditAnywhere)
	int puppets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int healthpacks;


};
