// Fill out your copyright notice in the Description page of Project Settings.


#include "Ruins_GameMode.h"
#include "MyRuinsGameStateBase.h"
#include "MyMainCharacterController.h"
#include "MyPawnCharacter.h"


ARuins_GameMode::ARuins_GameMode(){
	GameStateClass = AMyRuinsGameStateBase::StaticClass();
	PlayerControllerClass = AMyMainCharacterController::StaticClass();
	DefaultPawnClass = AMyPawnCharacter::StaticClass();

}

int ARuins_GameMode::GetPuppets() const
{
	return GetGameState<AMyRuinsGameStateBase>()->puppets;
}

int ARuins_GameMode::SetPuppets(int newNumberOfPuppets)
{
	return GetGameState<AMyRuinsGameStateBase>()->puppets = newNumberOfPuppets;
}

void ARuins_GameMode::StartPlay()
{
	GetWorld()->GetAuthGameMode()->GetGameState<AMyRuinsGameStateBase>();
	Super::StartPlay();
}


