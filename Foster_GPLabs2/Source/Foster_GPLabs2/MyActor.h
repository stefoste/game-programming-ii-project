// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class FOSTER_GPLABS2_API AMyActor : public AActor
{
	GENERATED_BODY()

	
	
public:	
	// Sets default values for this actor's properties
	AMyActor();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float speed;

	UPROPERTY(EditAnywhere)
	FVector EndPosition;

	UPROPERTY(VisibleAnywhere)
	FVector InitialPosition;

	UPROPERTY(VisibleAnywhere)
	FVector Target;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent * TheMesh;

	UFUNCTION(BlueprintPure)
		static FVector MoveTowards(
			UPARAM(DisplayName = "Current Location") FVector CurrentLocation,
			UPARAM(DisplayName = "Destination") FVector Destination,
			UPARAM(DisplayName = "Max Distance Delta") float MaxDistanceDelta);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
