// Fill out your copyright notice in the Description page of Project Settings.


#include "UActorComponent_Lab4.h"



// Sets default values for this component's properties
UUActorComponent_Lab4::UUActorComponent_Lab4()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	//PrimaryComponentTick.bStartWithTickEnabled = true;

	if (GetOwner() == nullptr) {
		TEXT ("This component doesn't have an owner");
	}
	else {
		moveableComponent = GetOwner()->FindComponentByClass<USceneComponent>();
	}
	// ...
}


// Called when the game starts
void UUActorComponent_Lab4::BeginPlay()
{
	Super::BeginPlay();
	moveableComponent->RegisterComponent();


	if (moveableComponent == nullptr) {
		if (GetOwner() == nullptr) {
			LocalTransform = GetOwner()->GetActorTransform();
		}
		else {
			moveableComponent = GetOwner()->FindComponentByClass<USceneComponent>();
		}
	}

	

	// ...
	
}


// Called every frame
void UUActorComponent_Lab4::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
	GEngine->AddOnScreenDebugMessage(-1, 200, FColor::Green, FString::Printf(TEXT("TICK")));

	// ...

	//FTransform transform = transform.children ;
	
}

/*void UUActorComponent_Lab4::RegisterComponent() {
	Super::RegisterComponent();


}*/

FVector UUActorComponent_Lab4::Scale(FVector CurrentScale, FVector NextScale, float s) {

	FVector NewScale = NextScale - CurrentScale;

	NewScale.Normalize();
	
	return NewScale * s;
}

FVector UUActorComponent_Lab4::Translate(FVector CurrentLocation, float MaxDistanceDelta) {
	/*FVector locate = CurrentLocation;

	float dist = locate.SizeSquared();
	float maxlocate = MaxDistanceDelta * MaxDistanceDelta;

	if (dist < maxlocate) {
		FVector backwards;
		return backwards;
	}
	else {
		locate.Normalize();
		return CurrentLocation + (CurrentLocation * MaxDistanceDelta);
	}*/

	FVector NewTranslation = FVector(CurrentLocation.X, CurrentLocation.Y, CurrentLocation.Z);
	float maxLocate = MaxDistanceDelta * MaxDistanceDelta;
	NewTranslation.Normalize(maxLocate);

	return NewTranslation;
}

FQuat UUActorComponent_Lab4::Rotation(FQuat CurrentRotationAxis) {
	FQuat NewRotation = FQuat(CurrentRotationAxis);
	NewRotation.Normalize();

	return NewRotation;
	
}



