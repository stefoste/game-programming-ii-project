 // Fill out your copyright notice in the Description page of Project Settings.


#include "Lab3Actor_1.h"

// Sets default values
ALab3Actor_1::ALab3Actor_1() : rotationSpeed(50), isMoving(true)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TheMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	TheMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube"));

	if (CubeVisualAsset.Succeeded()) {
		TheMesh->SetStaticMesh(CubeVisualAsset.Object);
		TheMesh->SetRelativeLocation(FVector(0.0, 0.0, 59.0));
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		CubeMaterial(TEXT("/Game/StarterContent/Materials/M_Rock_Sandstone"));

	if (CubeMaterial.Succeeded()) {
		TheMesh->SetMaterial(0, CubeMaterial.Object);
	}

	//PitchValue = 0.0f;
	//YawValue = 0.0f;
	//RollValue = 0.0f;

}

// Called when the game starts or when spawned
void ALab3Actor_1::BeginPlay()
{
	Super::BeginPlay();

	FTransform ft = GetOwner()->GetActorTransform();
	

}

// Called every frame
void ALab3Actor_1::Tick(float DeltaTime)
{
	if (isMoving) {

		Super::Tick(DeltaTime);

		float NewRotationSpeed = rotationSpeed * DeltaTime;

		//FRotator NewSpeed = FRotator(0, NewRotationSpeed, 0);

		//FRotator NewSpeed = GetActorRotation();

		//SetActorRotation(RotationAxis * NewRotationSpeed);

		FRotator newRotation = GetActorRotation() + FRotator(0, NewRotationSpeed, 0);

		SetActorRotation(newRotation);

	}

	else {
		rotationSpeed = 0;
	}
		//Where I left off, setting booleans to true if actors are moving. Set it to false and set rotation speed to 0, hopefully stopping all movement
	//For the bouncing actor, do the same thing. Take a look at the Children boolean stuff before doing this. Test it out on the rotation actors first to make sure it all works
	//Create blueprints for test

	

	//FQuat QuatRotation = FQuat(NewRotation);
	
	//SetActorRotation(QuatRotation, ETeleportType::None);

	//FRotator RotationSpeed = ALab3Actor_1::Speed(GetActorRotation(), rotationSpeed * DeltaTime);

	//FQuat QuatSpeed = FQuat(RotationSpeed);

}

//FVector Speed(
	//UPARAM(DisplayName = "Current Rotation") FVector CurrentRotation,
	//UPARAM(DisplayName = "Max Distance Delta") float MaxDistanceDelta);

//FRotator ALab3Actor_1::Speed(FRotator CurrentRotation, float MaxDistanceDelta) {
	//float speedMax = MaxDistanceDelta * MaxDistanceDelta;
	//return CurrentRotation * MaxDistanceDelta;
//}

