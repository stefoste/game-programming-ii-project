// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab3Actor_3.h"

// Sets default values
ALab3Actor_3::ALab3Actor_3() : bounce(200)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TheMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	TheMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere"));

	if (CubeVisualAsset.Succeeded()) {
		TheMesh->SetStaticMesh(CubeVisualAsset.Object);
		TheMesh->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		CubeMaterial(TEXT("/Game/StarterContent/Materials/M_Wood_Oak"));

	if (CubeMaterial.Succeeded()) {
		TheMesh->SetMaterial(0, CubeMaterial.Object);
	}

	isMoving = true;
}

// Called when the game starts or when spawned
void ALab3Actor_3::BeginPlay()
{
	Super::BeginPlay();
	//InitialPosition = GetActorLocation();
	//Target = EndPosition;
}

// Called every frame
void ALab3Actor_3::Tick(float DeltaTime)
{

	if (isMoving) {

		Super::Tick(DeltaTime);

		FVector CurrentLocation = GetActorLocation();

		if (bounce < CurrentLocation.Z) {
			direction = -1;
		}

		if (CurrentLocation.Z < 0) {
			direction = 1;
		}

		FVector* target = new FVector(CurrentLocation.X, CurrentLocation.Y, CurrentLocation.Z + (bounce * direction * DeltaTime));

		SetActorLocation(*target);
	}
	
	else {
		bounce = 0;
	}


	//if (GetActorLocation().Equals(Target)) {
	//	if (Target.Equals(EndPosition)) {
	//		Target = InitialPosition;
	//	}
	//	else {
	//		Target = EndPosition;
	//	}
	//}

	//else {
	//	FVector nextPos = ALab3Actor_3::MoveTowards(GetActorLocation(), Target, speed * DeltaTime);

	//	SetActorLocation(nextPos);
	//}
}

//FVector ALab3Actor_3::MoveTowards(FVector CurrentLocation, FVector Destination, float MaxDistanceDelta) {
//	FVector towards = Destination - CurrentLocation;
//
//	float sqDist = towards.SizeSquared();
//	float sqMax = MaxDistanceDelta * MaxDistanceDelta;
//
//	if (sqDist < sqMax) {
//		return Destination;
//	}
//	else {
//		towards.Normalize();
//		return CurrentLocation + (towards * MaxDistanceDelta);
//	}
//}

