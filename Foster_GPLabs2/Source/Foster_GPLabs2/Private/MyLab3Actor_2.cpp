// Fill out your copyright notice in the Description page of Project Settings.


#include "MyLab3Actor_2.h"

AMyLab3Actor_2::AMyLab3Actor_2()
{
	PrimaryActorTick.bCanEverTick = true; 
	
	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		CubeMaterial(TEXT("/Game/StarterContent/Materials/M_CobbleStone_Rough"));

	if (CubeMaterial.Succeeded()) {
		TheMesh->SetMaterial(0, CubeMaterial.Object);
	}
}

void AMyLab3Actor_2::BeginPlay() {
	Super::BeginPlay();
}

void AMyLab3Actor_2::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

//FRotator AMyLab3Actor_2::RotationCalculation(FRotator RotationAxis, float MaxDistanceDelta) {

//}



