// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Actor_2.h"

// Sets default values
ALab4Actor_2::ALab4Actor_2()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TheMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	TheMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cone"));

	if (CubeVisualAsset.Succeeded()) {
		TheMesh->SetStaticMesh(CubeVisualAsset.Object);
		TheMesh->SetRelativeLocation(FVector(0.0, 0.0, 59.0));
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		CubeMaterial(TEXT("/Game/StarterContent/Materials/M_Ground_Grass"));

	if (CubeMaterial.Succeeded()) {
		TheMesh->SetMaterial(0, CubeMaterial.Object);
	}

}

// Called when the game starts or when spawned
void ALab4Actor_2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab4Actor_2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

