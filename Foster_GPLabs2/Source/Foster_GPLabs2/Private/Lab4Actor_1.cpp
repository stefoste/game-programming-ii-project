// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Actor_1.h"

// Sets default values
ALab4Actor_1::ALab4Actor_1()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TheMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	TheMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube"));

	if (CubeVisualAsset.Succeeded()) {
		TheMesh->SetStaticMesh(CubeVisualAsset.Object);
		TheMesh->RegisterComponent();
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		CubeMaterial(TEXT("/Game/StarterContent/Materials/M_Ground_Grass"));

	if (CubeMaterial.Succeeded()) {
		TheMesh->SetMaterial(0, CubeMaterial.Object);
	}
}

// Called when the game starts or when spawned
void ALab4Actor_1::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab4Actor_1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

