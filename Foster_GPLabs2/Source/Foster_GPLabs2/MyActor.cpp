// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"

// Sets default values
AMyActor::AMyActor() : speed (100)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TheMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	TheMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		ConeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cone"));

	if (ConeVisualAsset.Succeeded()) {
		TheMesh->SetStaticMesh(ConeVisualAsset.Object);
		TheMesh->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		ConeMaterial(TEXT("/Game/StarterContent/Materials/M_Cobblestone_Pebble"));

	if (ConeMaterial.Succeeded()) {
		TheMesh->SetMaterial(0, ConeMaterial.Object);
	}

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();

	InitialPosition = GetActorLocation();
	Target = EndPosition;
	
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (GetActorLocation().Equals(Target)) {
		if (Target.Equals(EndPosition)) {
			Target = InitialPosition;
		}
		else {
			Target = EndPosition;
		}
	}

	else {
		FVector nextPos = AMyActor::MoveTowards(GetActorLocation(), Target, speed * DeltaTime);

		SetActorLocation(nextPos);
	}

}

/*void APawn::Tick(float maxSpeed)
{
	Super::Tick(maxSpeed);
}*/

FVector AMyActor::MoveTowards(FVector CurrentLocation, FVector Destination, float MaxDistanceDelta) {
	FVector towards = Destination - CurrentLocation;

	float sqDist = towards.SizeSquared();
	float sqMax = MaxDistanceDelta * MaxDistanceDelta;

	if (sqDist < sqMax) {
		return Destination;
	}
	else {
		towards.Normalize();
		return CurrentLocation + (towards * MaxDistanceDelta);
	}
}

