// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyPawn.generated.h"

UCLASS()
class FOSTER_GPLABS2_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawn();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxSpeed;

	UPROPERTY(EditAnywhere)
	FVector EndPosition;

	UPROPERTY(VisibleAnywhere)
	FVector InitialPosition;

	UPROPERTY(VisibleAnywhere)
	FVector Target;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent * TheMesh;

	UFUNCTION(BlueprintPure)
		static FVector Move(
			UPARAM(DisplayName = "Current Location") FVector CurrentLocation,
			UPARAM(DisplayName = "Destination") FVector Destination,
			UPARAM(DisplayName = "Max Distance Delta") float MaxDistanceDelta);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
