// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UActorComponent_Lab4.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FOSTER_GPLABS2_API UUActorComponent_Lab4 : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUActorComponent_Lab4();
	
	UPROPERTY(EditAnywhere)
	AActor* Parent;

	UPROPERTY(EditAnywhere)
		TArray<AActor*> children;

	UPROPERTY(EditAnywhere)
		UActorComponent* moveableComponent;

	UFUNCTION(CallInEditor)
		static FVector Translate(
			UPARAM(DisplayName = "Current Location") FVector CurrentLocation,
			UPARAM(DisplayName = "MaxDistanceDelta") float MaxDistanceDelta
	);

	UFUNCTION(CallInEditor)
		static FVector Scale(
			UPARAM(DisplayName = "Current Scale") FVector CurrentScale,
			UPARAM(DisplayName = "Next Scale") FVector NextScale,
			UPARAM(DisplayName = "Scale Number") float s
		);

	UFUNCTION(CallInEditor)
		static FQuat Rotation(
			UPARAM(DisplayName = "Current Rotation Axis") FQuat CurrentRotationAxis
		);

	UPROPERTY(EditAnywhere)
		FTransform LocalTransform;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//virtual void RegisterComponent();
};
