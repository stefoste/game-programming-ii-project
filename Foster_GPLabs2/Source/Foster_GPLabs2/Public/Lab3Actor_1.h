// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Lab3Actor_1.generated.h"

UCLASS()
class FOSTER_GPLABS2_API ALab3Actor_1 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALab3Actor_1();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent * TheMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float rotationSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator RotationAxis;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool isMoving;

	//Startand Stopping all movement
	//UNCTION(BlueprintCallable, Category = "Movement")
		//id Motion();

	//UPROPERTY(EditAnywhere, Category = Movement)
	//float PitchValue;

	//UPROPERTY(EditAnywhere, Category = Movement)
	//float YawValue;

	//UPROPERTY(EditAnywhere, Category = Movement)
	//float RollValue;
	
	/*UFUNCTION(BlueprintPure)
		FRotator Speed(
			UPARAM(DisplayName = "Current Rotation") FRotator CurrentRotation,
			UPARAM(DisplayName = "Max Distance Delta") float MaxDistanceDelta);
	*/

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override; 

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	

};
