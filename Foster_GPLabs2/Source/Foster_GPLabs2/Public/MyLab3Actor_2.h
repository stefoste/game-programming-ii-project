// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Lab3Actor_1.h"
#include "MyLab3Actor_2.generated.h"

/**
 * 
 */
UCLASS()
class FOSTER_GPLABS2_API AMyLab3Actor_2 : public ALab3Actor_1
{
	GENERATED_BODY()

public:
	AMyLab3Actor_2();


	/*UPROPERTY(EditAnywhere)
		static FRotator RotationCalculation(
			UPARAM(DisplayName = "RotationAxis") FRotator RotationAxis,
			UPARAM(DisplayName = "Max Distance Delta") float MaxDistanceDelta);
	*/
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
};


