// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Lab4Actor_1.generated.h"

UCLASS()
class FOSTER_GPLABS2_API ALab4Actor_1 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALab4Actor_1();

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* TheMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
