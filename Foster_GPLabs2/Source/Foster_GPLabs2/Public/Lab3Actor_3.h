// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Lab3Actor_3.generated.h"

UCLASS()
class FOSTER_GPLABS2_API ALab3Actor_3 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALab3Actor_3();
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* TheMesh;

	UPROPERTY(EditAnywhere)
		float bounce;

	UPROPERTY(EditAnywhere)
		float direction;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool isMoving;

	//Start and Stop Motion
//FUNCTION(BlueprintCallable, Category = "Movement")
	//oid Motion();

	//UPROPERTY(EditAnywhere)
	//	FVector EndPosition;

	//UPROPERTY(VisibleAnywhere)
	//	FVector InitialPosition;

	//UPROPERTY(VisibleAnywhere)
	//	FVector Target;

	//UFUNCTION(BlueprintPure)
	//	static FVector MoveTowards(
	//		UPARAM(DisplayName = "Current Location") FVector CurrentLocation,
	//		UPARAM(DisplayName = "Destination") FVector Destination,
	//		UPARAM(DisplayName = "Max Distance Delta") float MaxDistanceDelta);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
