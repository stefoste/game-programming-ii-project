// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawn.h"

// Sets default values
AMyPawn::AMyPawn() : MaxSpeed (90)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TheMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	TheMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere"));

	if (SphereVisualAsset.Succeeded()) {
		TheMesh->SetStaticMesh(SphereVisualAsset.Object);
		TheMesh->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		SphereMaterial(TEXT("/Game/StarterContent/Materials/M_Rock_Marble_Polished"));

	if (SphereMaterial.Succeeded()) {
		TheMesh->SetMaterial(0, SphereMaterial.Object);
	}
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();

	InitialPosition = GetActorLocation();
	Target = EndPosition;
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(GetActorLocation().Equals(Target)) {
		if (Target.Equals(EndPosition)) {
			Target = InitialPosition;
		}
		else {
			Target = EndPosition;
		}
	}

	else {
		FVector nextPos = AMyPawn::Move(GetActorLocation(), Target, MaxSpeed);

		SetActorLocation(nextPos);
	}

}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
 
FVector AMyPawn::Move(FVector CurrentLocation, FVector Destination, float MaxDistanceDelta) {
	FVector towards = Destination - CurrentLocation;

	float sqDist = towards.SizeSquared();
	float sqMax = MaxDistanceDelta * MaxDistanceDelta;

	if (sqDist < sqMax) {
		return Destination;
	}
	else {
		towards.Normalize();
		return CurrentLocation + (towards * MaxDistanceDelta);
	}
}

