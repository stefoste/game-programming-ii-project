// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Foster_GPLabs2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FOSTER_GPLABS2_API AFoster_GPLabs2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
